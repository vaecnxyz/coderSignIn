package com.xusong.service;

import com.xusong.entity.JsonResult;
import com.xusong.entity.User;

public interface IUserService {
    JsonResult query(User user);
}
