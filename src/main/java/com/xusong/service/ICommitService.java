package com.xusong.service;

import com.sun.deploy.net.HttpResponse;
import com.xusong.entity.Commit;
import com.xusong.entity.JsonResult;
import com.xusong.entity.User;
import org.springframework.http.HttpRequest;

public interface ICommitService {
    JsonResult query();

    JsonResult save(Commit commit);

    JsonResult load(Integer id);
}
