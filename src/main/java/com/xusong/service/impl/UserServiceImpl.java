package com.xusong.service.impl;

import com.xusong.dao.UserDao;
import com.xusong.entity.JsonResult;
import com.xusong.entity.User;
import com.xusong.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements IUserService {
    @Autowired
    UserDao userDao;

    public JsonResult query(User user) {
        User userReal = userDao.query(user.getId());
        if(userReal==null){
            return new JsonResult(false,"用户名错误",null);
        }else{
            if(userReal.getPassword().equals(user.getPassword())){
                return new JsonResult(true,"登录成功",null);
            }else{
                return new JsonResult(false,"密码错误",null);
            }
        }
    }
}
