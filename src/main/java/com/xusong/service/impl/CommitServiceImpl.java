package com.xusong.service.impl;

import com.sun.deploy.net.HttpResponse;
import com.xusong.dao.CommitDao;
import com.xusong.dao.UserDao;
import com.xusong.entity.Commit;
import com.xusong.entity.JsonResult;
import com.xusong.entity.User;
import com.xusong.service.ICommitService;
import com.xusong.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Service;

import javax.jms.Session;
import javax.persistence.metamodel.SetAttribute;
import javax.servlet.http.Cookie;
import java.util.Date;

/**
 * @author Administrator
 */
@Service
public class CommitServiceImpl implements ICommitService {
    @Autowired
    private CommitDao commitDao;

    public JsonResult query() {
        return new JsonResult(true,"查询成功",commitDao.query());
    }

    public JsonResult save(Commit commit) {
        commit.setCreateTime(new Date());
        int flag=commitDao.save(commit);
        if(flag==1){
            JsonResult jsonResult=new JsonResult(true,"提交成功",commit);
            return jsonResult;
        }else{
            return new JsonResult(false,"提交失败",null);
        }
    }

    public JsonResult load(Integer id) {
        return new JsonResult(true,"查询成功",commitDao.load(id));
    }
}
