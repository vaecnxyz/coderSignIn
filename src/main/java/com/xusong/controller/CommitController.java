package com.xusong.controller;

import com.xusong.entity.Commit;
import com.xusong.entity.JsonResult;
import com.xusong.service.ICommitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

/**
 * @author Administrator
 */
@RestController
@RequestMapping("/commit")
public class CommitController {
    @Autowired
    private ICommitService commitService;

    @RequestMapping("/save")
    public JsonResult save(Commit commit) {
        return commitService.save(commit);
    }

    @RequestMapping("/query")
    public JsonResult query() {
        return commitService.query();
    }

    @RequestMapping("/load/{id}")
    public JsonResult load(@PathVariable("id") Integer id) {
        return commitService.load(id);
    }

    @RequestMapping("/imgUpload")
    public JsonResult load(MultipartFile img, HttpServletRequest request) {
        String name = System.currentTimeMillis() + img.getOriginalFilename();
        try {
            // 文件保存路径
            String filePath = request.getSession().getServletContext().getRealPath("/") + "imgUpload/" + name;
            // 转存文件
            img.transferTo(new File(filePath));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JsonResult(true, "上传成功", name);
    }
}
