package com.xusong.controller;

import com.xusong.entity.JsonResult;
import com.xusong.entity.User;
import com.xusong.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Administrator
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUserService userService;

    @RequestMapping(value = "/check.do")
    public JsonResult query(User user){
        return userService.query(user);
    }
}
