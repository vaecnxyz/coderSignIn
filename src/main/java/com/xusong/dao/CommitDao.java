package com.xusong.dao;

import com.xusong.entity.Commit;

import java.util.List;

/**
 * @author Administrator
 */
public interface CommitDao {
    List<Commit> query();

    int save(Commit commit);

    Commit load(Integer id);
}
