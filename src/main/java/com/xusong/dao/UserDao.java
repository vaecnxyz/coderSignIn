package com.xusong.dao;

import com.xusong.entity.User;

/**
 * @author Administrator
 */
public interface UserDao {
    User query(String id);
}
